import pyvisa as visa
from enum import Enum
from typing import List

class Data_Format(str, Enum):
    ascii = 'ASC'
    binary = 'PACK'

class List_Type(str, Enum):
    data = 'DLIST'
    control = 'CLIST'
    user_std = 'ULIST'
    user_filter = 'FLIST'
    user_mapping = 'MLIST'

class UWB_Standard (str, Enum):
    nonhrp = 'NONHRP'
    bprf = 'BPRF'
    hprf = 'HPRF'

class UWB_Payload_Source (str, Enum):
    pn9 = 'PN9'
    pn11 = 'PN11'
    pn15 = 'PN15'
    pn20 = 'PN20'
    pn16 = 'PN16'
    pn21 = 'PN21'
    pn23 = 'PN23'
    one = 'ONE'
    zero = 'ZERO'
    datalist = 'DLIS'
    pattern = 'PATT'

class UWB_Sync_Length(str, Enum):
    sl_16 = 'SL_16'
    sl_64 = 'SL_64'
    sl_1024 = 'SL_1024'
    sl_4096 = 'SL_4096'

class UWB_Delta_Length(str, Enum):
    dl_4 = 'DL_4'
    dl_16 = 'SL_16'
    dl_64 = 'SL_64'

class UWB_MAC_FCS_length(str, Enum):
    mfl_2 = 'MFL_2'
    mfl_4 = 'MFL_4'
    
class UWB_Hop_Bursts(str, Enum):
    hb_2 = 'HB_2'
    hb_8 = 'HB_8'
    hb_32 = 'HB_32'

class UWB_Chips_Burst(str, Enum):
    cp_1 = 'CPB_1'
    cp_2 = 'CPB_2'
    cp_4 = 'CPB_4'
    cp_8 = 'CPB_8'
    cp_16 = 'CPB_16'
    cp_32 = 'CPB_32'
    cp_64 = 'CPB_64'
    cp_128 = 'CPB_128'
    cp_512 = 'CPB_512'

class UWB_Active_Segment_Length(str, Enum):
    asl_16 = 'ASL_16'
    asl_32 = 'ASL_32'
    asl_64 = 'ASL_64'
    asl_128 = 'ASL_128'
    asl_512 = 'ASL_512'
    asl_1024 = 'ASL_1024'
    asl_2048 = 'ASL_2048'

class UWB_Trigger_Mode(str, Enum):
    auto = 'AUTO'
    retrigger = 'RETR'
    armed_auto = 'AAUT'
    armed_retrigger = 'ARET'
    single = 'SING'

class UWB_Trigger_Source(str, Enum):
    internal = 'INT'
    external = 'EXT'
    ext_glob1 = 'EGT1'
    ext_glob2 = 'EGT2'
    int_bb1 = 'INTA'
    int_bb2 = 'INTB'

class SMWxxx():
    def __init__(self, resource: str, reset:bool = False):
        rm = visa.ResourceManager()
        self.rsrc = rm.open_resource(resource)
        if reset:
            self.reset()
        
    def reset(self):
        self.rsrc.write('*RST')

    def iden(self):
        return self.rsrc.query('*IDN?')

    def opc_status(self):
        return self.rsrc.query('*OPC?').strip() == '1'

    def rf_frequency(self, source:int = 1, frequency_hz:float = 1e9):
        self.rsrc.write(f'SOUR{source}:FREQ {frequency_hz}')
    
    def rf_power(self, source:int = 1, power_dbm:float = -30):
        self.rsrc.write(f'SOUR{source}:POW {power_dbm}')

    def rf_output(self, source:int = 1, enable:bool = False):
        self.rsrc.write(f'OUTP{source}:STAT {1 if enable else 0}')

    def data_format(self, format:Data_Format):
        self.rsrc.write(f'FORM:DATA {format}')

    def digital_modulation_list_select(self, list:List_Type, file:str, source:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:DM:{list}:SEL "{file}"')

    def digital_modulation_list_data(self, data:List[int], source:int = 1):
        self.rsrc.write_binary_values(f'SOUR{source}:BB:DM:DLIST:DATA ', bytearray(data),
            datatype='s', is_big_endian=False)

    def uwb_preset(self, source:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:PRES')

    def uwb_load_waveform(self, source:int = 1, waveform:str = ''):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:SETT:LOAD {waveform}')
    
    def uwb_state(self, source:int = 1, enable:bool = False):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:STAT {1 if enable else 0}')
    
    def uwb_std(self, source:int = 1, std:UWB_Standard = UWB_Standard.hprf):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:STD {std}')

    def uwb_bandwidth(self, source:int = 1):
        return float(self.rsrc.query(f'SOUR{source}:BB:HUWB:BWID?'))

    def uwb_channel(self, source:int = 1, channel:int = 9):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:CNUM {channel}')

    def uwb_idle_interval(self, source:int = 1, gap_us:float = 50):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:IINT {gap_us}')

    def uwb_seq_len(self, source:int = 1, frames:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:SLEN {frames}')

    def uwb_mac_frame_check(self, source:int = 1, enable:bool = False, 
        length:UWB_MAC_FCS_length=UWB_MAC_FCS_length.mfl_2):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:MCS:STAT {1 if enable else 0}')
        if enable:
            self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:MFL {length}')

    def uwb_phy_data_rate(self, source:int = 1, rate:str = 'BMHP'):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:PHR:DRM {rate}')

    def uwb_header_data_length(self, source:int = 1, length:int = 20):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:DALE {length}')

    def uwb_payload(self, source:int = 1, payload_source:UWB_Payload_Source = UWB_Payload_Source.pn9, 
                    data_list:str = '', pattern:int = 0, bit_count:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:DATA {payload_source}')
        if payload_source == UWB_Payload_Source.datalist:
            self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:DATA:DSEL "{data_list}"')
        elif payload_source == UWB_Payload_Source.pattern:
            self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:DATA:PATT #H{pattern:X},{bit_count}')

    def uwb_sync_length(self, source:int = 1, length:UWB_Sync_Length = UWB_Sync_Length.sl_64):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:SYNL {length}')

    def uwb_delta_length(self, source:int = 1, length:UWB_Delta_Length = UWB_Delta_Length.dl_16):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:DLEN {length}')

    def uwb_sfd(self, source:int = 1, index:int=5):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:SFD SFD_{index}')
    
    def uwb_code_index(self, source:int = 1, index:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:CIND CI_{index}')

    def uwb_packet_configuration(self, source:int = 1, index:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:STS:PC SPC_{index}')
    
    def uwb_hop_bursts(self, source:int = 1, bursts:UWB_Hop_Bursts = UWB_Hop_Bursts.hb_2):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:HOPB {bursts}')
    
    def uwb_chips_burst(self, source:int = 1, chips:UWB_Chips_Burst = UWB_Chips_Burst.cp_8):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:FCON:CPB {chips}')

    def uwb_active_segment_length(self, source:int = 1, length:UWB_Active_Segment_Length = UWB_Active_Segment_Length.asl_32):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:ASL {length}')

    # setup single trigger
    def uwb_trigger_mode(self, mode:UWB_Trigger_Mode, source:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:TRIG:SEQ {mode}')
    
    def uwb_trigger_source(self, trig_src:UWB_Trigger_Source, source:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:TRIG:SOUR {trig_src}')

    def uwb_manual_trigger(self, source:int = 1):
        self.rsrc.write(f'SOUR{source}:BB:HUWB:TRIG:EXEC')
    
    def uwb_trigger_running(self, source:int = 1):
        return self.rsrc.query(f'SOUR{source}:BB:HUWB:TRIG:RMOD?').strip() == 'RUN'

    