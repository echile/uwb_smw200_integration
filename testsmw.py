import instruments.smwxxx as smwxxx
import time

def bprf_3(sg:smwxxx.SMWxxx):
    sg.uwb_preset()
    sg.uwb_std(1, smwxxx.UWB_Standard.bprf)
    sg.uwb_channel(1, 9)

    # packet
    sg.uwb_code_index(1, 7)
    sg.uwb_packet_configuration(1, 1)
    
    # sync
    sg.uwb_sync_length(1, smwxxx.UWB_Sync_Length.sl_64)
    sg.uwb_delta_length(1, smwxxx.UWB_Delta_Length.dl_4)
    sg.uwb_sfd(1, 2)
    
    # data
    sg.uwb_payload(1, smwxxx.UWB_Payload_Source.pn9)
    sg.uwb_mac_frame_check(1, True)
    sg.uwb_phy_data_rate(1)

    # stat

    # state
    sg.uwb_state(1, True)
    # rf
    sg.rf_power(1, -40)
    sg.rf_frequency(1, 7987.2e6)
    sg.rf_output(1, True)


def main():
    sg = smwxxx.SMWxxx('TCPIP::10.41.99.44::inst0::INSTR')
    sg.rsrc.timeout = 10000
    sg.reset()
    print(sg.iden())

    while not sg.opc_status():
        time.sleep(1)

    sg.uwb_preset()

    hw = 1

    # setup data file
    data = [i for i in range(18)]
    sg.data_format(smwxxx.Data_Format.binary)
    sg.digital_modulation_list_select(smwxxx.List_Type.data, '/var/user/temp_uwb')
    sg.digital_modulation_list_data(data)

    sg.uwb_std(hw, smwxxx.UWB_Standard.bprf)
    sg.uwb_packet_configuration(hw, index=0)
    sg.uwb_channel(hw, 9)
    sg.uwb_code_index(hw, index=9)
    sg.uwb_seq_len(hw, frames=1)
    sg.uwb_idle_interval(hw, gap_us=1000)

    # sync
    sg.uwb_sync_length(hw, length=smwxxx.UWB_Sync_Length.sl_64)
    sg.uwb_delta_length(hw, length=smwxxx.UWB_Delta_Length.dl_4)
    sg.uwb_sfd(hw, index=0)

    print(sg.uwb_bandwidth())

    # Data
    sg.uwb_hop_bursts(hw, bursts=smwxxx.UWB_Hop_Bursts.hb_2)
    sg.uwb_chips_burst(hw, chips=smwxxx.UWB_Chips_Burst.cp_8)
    sg.uwb_mac_frame_check(hw, enable=True, length=smwxxx.UWB_MAC_FCS_length.mfl_2)
    sg.uwb_header_data_length(hw, length=18)
    sg.uwb_payload(hw, payload_source=smwxxx.UWB_Payload_Source.datalist, data_list='/var/user/temp_uwb')
    
    # Trigger
    sg.uwb_trigger_mode(smwxxx.UWB_Trigger_Mode.single)
    sg.uwb_trigger_source(smwxxx.UWB_Trigger_Source.internal)

    sg.uwb_state(1, True)
    sg.opc_status()

    sg.rf_power(1, -40)
    sg.rf_frequency(1, 7987.2e6)
    sg.rf_output(1, True)
    sg.opc_status()

    runs = 200
    for i in range(runs):
        data[1] = i
        sg.digital_modulation_list_data(data)
        sg.uwb_manual_trigger()
        sg.opc_status()
        while sg.uwb_trigger_running():
            time.sleep(.01)
        
        

    print('place holder')

if __name__ == '__main__':
    main()