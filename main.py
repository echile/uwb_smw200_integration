# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui\main.ui'
#
# Created by: PyQt5 UI code generator 5.15.1
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(282, 578)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.configure_button = QtWidgets.QPushButton(self.centralwidget)
        self.configure_button.setGeometry(QtCore.QRect(90, 490, 93, 28))
        self.configure_button.setObjectName("configure_button")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 10, 254, 464))
        self.widget.setObjectName("widget")
        self.gridLayout = QtWidgets.QGridLayout(self.widget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.bandwidth_box = QtWidgets.QComboBox(self.widget)
        self.bandwidth_box.setEditable(True)
        self.bandwidth_box.setObjectName("bandwidth_box")
        self.bandwidth_box.addItem("")
        self.gridLayout.addWidget(self.bandwidth_box, 0, 2, 1, 1)
        self.label_19 = QtWidgets.QLabel(self.widget)
        self.label_19.setObjectName("label_19")
        self.gridLayout.addWidget(self.label_19, 0, 3, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.channel_box = QtWidgets.QComboBox(self.widget)
        self.channel_box.setEditable(True)
        self.channel_box.setObjectName("channel_box")
        self.channel_box.addItem("")
        self.channel_box.addItem("")
        self.gridLayout.addWidget(self.channel_box, 1, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.data_rate_box = QtWidgets.QComboBox(self.widget)
        self.data_rate_box.setEditable(True)
        self.data_rate_box.setObjectName("data_rate_box")
        self.data_rate_box.addItem("")
        self.gridLayout.addWidget(self.data_rate_box, 2, 2, 1, 1)
        self.label_20 = QtWidgets.QLabel(self.widget)
        self.label_20.setObjectName("label_20")
        self.gridLayout.addWidget(self.label_20, 2, 3, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.widget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.frequency_box = QtWidgets.QComboBox(self.widget)
        self.frequency_box.setEditable(True)
        self.frequency_box.setObjectName("frequency_box")
        self.frequency_box.addItem("")
        self.gridLayout.addWidget(self.frequency_box, 3, 2, 1, 1)
        self.label_21 = QtWidgets.QLabel(self.widget)
        self.label_21.setObjectName("label_21")
        self.gridLayout.addWidget(self.label_21, 3, 3, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.widget)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 4, 0, 1, 1)
        self.Mode_box = QtWidgets.QComboBox(self.widget)
        self.Mode_box.setEditable(True)
        self.Mode_box.setObjectName("Mode_box")
        self.Mode_box.addItem("")
        self.Mode_box.addItem("")
        self.gridLayout.addWidget(self.Mode_box, 4, 2, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.widget)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 5, 0, 1, 1)
        self.packets_rx_box = QtWidgets.QSpinBox(self.widget)
        self.packets_rx_box.setObjectName("packets_rx_box")
        self.gridLayout.addWidget(self.packets_rx_box, 5, 2, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.widget)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 6, 0, 1, 1)
        self.packets_rx_gap_box = QtWidgets.QSpinBox(self.widget)
        self.packets_rx_gap_box.setEnabled(True)
        self.packets_rx_gap_box.setObjectName("packets_rx_gap_box")
        self.gridLayout.addWidget(self.packets_rx_gap_box, 6, 2, 1, 1)
        self.label_22 = QtWidgets.QLabel(self.widget)
        self.label_22.setObjectName("label_22")
        self.gridLayout.addWidget(self.label_22, 6, 3, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.widget)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 7, 0, 1, 1)
        self.payload_line = QtWidgets.QLineEdit(self.widget)
        self.payload_line.setObjectName("payload_line")
        self.gridLayout.addWidget(self.payload_line, 7, 2, 1, 2)
        self.label_9 = QtWidgets.QLabel(self.widget)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 8, 0, 1, 1)
        self.payload_max_fcs_box = QtWidgets.QComboBox(self.widget)
        self.payload_max_fcs_box.setEditable(True)
        self.payload_max_fcs_box.setObjectName("payload_max_fcs_box")
        self.payload_max_fcs_box.addItem("")
        self.payload_max_fcs_box.addItem("")
        self.gridLayout.addWidget(self.payload_max_fcs_box, 8, 2, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.widget)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 9, 0, 1, 1)
        self.payload_source_box = QtWidgets.QComboBox(self.widget)
        self.payload_source_box.setEditable(True)
        self.payload_source_box.setObjectName("payload_source_box")
        self.payload_source_box.addItem("")
        self.gridLayout.addWidget(self.payload_source_box, 9, 2, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.widget)
        self.label_11.setObjectName("label_11")
        self.gridLayout.addWidget(self.label_11, 10, 0, 1, 1)
        self.phr_data_rate_box = QtWidgets.QComboBox(self.widget)
        self.phr_data_rate_box.setEditable(True)
        self.phr_data_rate_box.setObjectName("phr_data_rate_box")
        self.phr_data_rate_box.addItem("")
        self.gridLayout.addWidget(self.phr_data_rate_box, 10, 2, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.widget)
        self.label_12.setObjectName("label_12")
        self.gridLayout.addWidget(self.label_12, 11, 0, 1, 1)
        self.phr_rate_box = QtWidgets.QComboBox(self.widget)
        self.phr_rate_box.setEditable(True)
        self.phr_rate_box.setObjectName("phr_rate_box")
        self.phr_rate_box.addItem("")
        self.gridLayout.addWidget(self.phr_rate_box, 11, 2, 1, 1)
        self.label_23 = QtWidgets.QLabel(self.widget)
        self.label_23.setObjectName("label_23")
        self.gridLayout.addWidget(self.label_23, 11, 3, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.widget)
        self.label_13.setObjectName("label_13")
        self.gridLayout.addWidget(self.label_13, 12, 0, 1, 1)
        self.power_rx_rate = QtWidgets.QComboBox(self.widget)
        self.power_rx_rate.setEditable(True)
        self.power_rx_rate.setObjectName("power_rx_rate")
        self.power_rx_rate.addItem("")
        self.gridLayout.addWidget(self.power_rx_rate, 12, 2, 1, 1)
        self.label_24 = QtWidgets.QLabel(self.widget)
        self.label_24.setObjectName("label_24")
        self.gridLayout.addWidget(self.label_24, 12, 3, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.widget)
        self.label_14.setObjectName("label_14")
        self.gridLayout.addWidget(self.label_14, 13, 0, 1, 1)
        self.preamble_code_ind_box = QtWidgets.QComboBox(self.widget)
        self.preamble_code_ind_box.setEditable(True)
        self.preamble_code_ind_box.setObjectName("preamble_code_ind_box")
        self.preamble_code_ind_box.addItem("")
        self.gridLayout.addWidget(self.preamble_code_ind_box, 13, 2, 1, 1)
        self.label_15 = QtWidgets.QLabel(self.widget)
        self.label_15.setObjectName("label_15")
        self.gridLayout.addWidget(self.label_15, 14, 0, 1, 2)
        self.preamble_len_box = QtWidgets.QComboBox(self.widget)
        self.preamble_len_box.setEditable(True)
        self.preamble_len_box.setObjectName("preamble_len_box")
        self.preamble_len_box.addItem("")
        self.gridLayout.addWidget(self.preamble_len_box, 14, 2, 1, 1)
        self.label_16 = QtWidgets.QLabel(self.widget)
        self.label_16.setObjectName("label_16")
        self.gridLayout.addWidget(self.label_16, 15, 0, 1, 2)
        self.prf_mode_box = QtWidgets.QComboBox(self.widget)
        self.prf_mode_box.setEditable(True)
        self.prf_mode_box.setObjectName("prf_mode_box")
        self.prf_mode_box.addItem("")
        self.gridLayout.addWidget(self.prf_mode_box, 15, 2, 1, 1)
        self.label_17 = QtWidgets.QLabel(self.widget)
        self.label_17.setObjectName("label_17")
        self.gridLayout.addWidget(self.label_17, 16, 0, 1, 1)
        self.sfd_type_box = QtWidgets.QComboBox(self.widget)
        self.sfd_type_box.setEditable(True)
        self.sfd_type_box.setObjectName("sfd_type_box")
        self.sfd_type_box.addItem("")
        self.gridLayout.addWidget(self.sfd_type_box, 16, 2, 1, 1)
        self.label_18 = QtWidgets.QLabel(self.widget)
        self.label_18.setObjectName("label_18")
        self.gridLayout.addWidget(self.label_18, 17, 0, 1, 1)
        self.sts_pckt_struct_box = QtWidgets.QComboBox(self.widget)
        self.sts_pckt_struct_box.setEditable(True)
        self.sts_pckt_struct_box.setObjectName("sts_pckt_struct_box")
        self.sts_pckt_struct_box.addItem("")
        self.gridLayout.addWidget(self.sts_pckt_struct_box, 17, 1, 1, 2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 282, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.configure_button.clicked.connect(print('hello'))
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.configure_button.setText(_translate("MainWindow", "Configure"))
        self.label.setText(_translate("MainWindow", "Bandwidth"))
        self.bandwidth_box.setItemText(0, _translate("MainWindow", "499.8"))
        self.label_19.setText(_translate("MainWindow", "MHz"))
        self.label_2.setText(_translate("MainWindow", "Channel"))
        self.channel_box.setItemText(0, _translate("MainWindow", "5"))
        self.channel_box.setItemText(1, _translate("MainWindow", "9"))
        self.label_3.setText(_translate("MainWindow", "Data Rate"))
        self.data_rate_box.setItemText(0, _translate("MainWindow", "6.8"))
        self.label_20.setText(_translate("MainWindow", "Mbps"))
        self.label_4.setText(_translate("MainWindow", "Frequency"))
        self.frequency_box.setItemText(0, _translate("MainWindow", "7987.2"))
        self.label_21.setText(_translate("MainWindow", "MHz"))
        self.label_5.setText(_translate("MainWindow", "Mode"))
        self.Mode_box.setItemText(0, _translate("MainWindow", "802.15.4"))
        self.Mode_box.setItemText(1, _translate("MainWindow", "802.15.4z"))
        self.label_6.setText(_translate("MainWindow", "Packets RX"))
        self.label_7.setText(_translate("MainWindow", "Packets RX Gap"))
        self.label_22.setText(_translate("MainWindow", "ms"))
        self.label_8.setText(_translate("MainWindow", "Payload"))
        self.label_9.setText(_translate("MainWindow", "Payload MAC FCS"))
        self.payload_max_fcs_box.setItemText(0, _translate("MainWindow", "True"))
        self.payload_max_fcs_box.setItemText(1, _translate("MainWindow", "False"))
        self.label_10.setText(_translate("MainWindow", "Payload Source"))
        self.payload_source_box.setItemText(0, _translate("MainWindow", "List"))
        self.label_11.setText(_translate("MainWindow", "PHR Data Rate"))
        self.phr_data_rate_box.setItemText(0, _translate("MainWindow", "DRMDR"))
        self.label_12.setText(_translate("MainWindow", "PHR Rate"))
        self.phr_rate_box.setItemText(0, _translate("MainWindow", "0.85"))
        self.label_23.setText(_translate("MainWindow", "Mbps"))
        self.label_13.setText(_translate("MainWindow", "Power RX"))
        self.power_rx_rate.setItemText(0, _translate("MainWindow", "-40"))
        self.label_24.setText(_translate("MainWindow", "dBm"))
        self.label_14.setText(_translate("MainWindow", "Preamble Code Index"))
        self.preamble_code_ind_box.setItemText(0, _translate("MainWindow", "9"))
        self.label_15.setText(_translate("MainWindow", "Preamble Length"))
        self.preamble_len_box.setItemText(0, _translate("MainWindow", "128"))
        self.label_16.setText(_translate("MainWindow", "PRF Mode"))
        self.prf_mode_box.setItemText(0, _translate("MainWindow", "BPRF"))
        self.label_17.setText(_translate("MainWindow", "SFD Type"))
        self.sfd_type_box.setItemText(0, _translate("MainWindow", "0"))
        self.label_18.setText(_translate("MainWindow", "STS Packet Structute"))
        self.sts_pckt_struct_box.setItemText(0, _translate("MainWindow", "0"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
